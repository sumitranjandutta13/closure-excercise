const counterFactory = require('../counterFactory');

const counter = counterFactory();

console.log(counter.increment());
console.log(counter.decrement());