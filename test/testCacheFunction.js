const cacheFunction = require('../cacheFunction');

const value = cacheFunction(function(num){
    console.log("CallBack Work for " + num);
    return num ? num*num : num;
})

for(let i = 1; i<=5; i++){
    value(i);
}